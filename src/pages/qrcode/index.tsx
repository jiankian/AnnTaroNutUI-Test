import {BasicComponent} from "@nutui/nutui-react-taro/dist/types/utils/typings";
import {View} from "@tarojs/components";
import CodeCreator from "taro-code-creator";
import Taro from "@tarojs/taro";
import {QRCode} from "taro-code";


function Qrcode(props: BasicComponent) {
    return <View
        id={props.id}
        className={props.className}
        style={props.style}
    >
        {/*不能同时展示二维码和条形码 只能二选一 二维码引入占用24KB */}
        <CodeCreator
            type={'qr'}
            codeText="http://www.anline.cn"
            size={120}
        />
        {/*<CodeCreator*/}
        {/*    type="bar"*/}
        {/*    width={400} height={100} codeText="test" />*/}
        {/* CodeCreator 二维码超出当前屏幕视图时 会悬浮在屏幕顶层显示 */}
        <QRCode
            text={'http://www.anline.cn'}
            size={Taro.getWindowInfo()?.windowWidth * (2 / 5) || 200}
            scale={4}
            errorCorrectLevel='M'
            typeNumber={2}
        />
    </View>
}

export default Qrcode;

