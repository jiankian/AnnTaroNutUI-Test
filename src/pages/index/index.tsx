import React from 'react'
import {Image as TaroImage, Text, View} from '@tarojs/components'
import {Avatar, Button, Form, Image, Popup, Tabbar, Uploader} from "@nutui/nutui-react-taro"
import './index.less'
import Taro from "@tarojs/taro";
import TaroPackageJson from "@tarojs/taro/package.json";
import axios from "axios";

// import packageJson from 'package.json' // taro 无法引用此文件

function Index() {


    const [popupViewVisible, setPopupViewVisible] = React.useState<boolean>(false)

    const [form] = Form.useForm()

    return (
        <View className="nutui-react-demo">
            <View className="column">
                <Text>
                    欢迎使用 NutUI React 开发 Taro 多端项目。
                </Text>
                <Text style={
                    {
                        color: '#999',
                        fontSize: 'small'
                    }
                }>
                    Taro版本号：{TaroPackageJson.version}
                </Text>
            </View>
            <View>
                <Button
                    onClick={() => {
                        Taro.navigateTo({
                            url: '/pages/cascader/index',
                        })
                    }}
                >
                    Cascader
                </Button>
            </View>
            <View>
                <Button
                    onClick={() => {
                        Taro.navigateTo({
                            url: '/pages/qrcode/index',
                        })
                    }}
                >
                    QRCode
                </Button>
            </View>
            <View className="index"
                  style={
                      {
                          display: 'flex',
                          flexDirection: 'column',
                      }
                  }
            >
                <Button type="primary" className="btn">
                    NutUI React Button
                </Button>

                <Form
                    form={form}
                    footer={
                        <View
                        >
                            <div
                            >
                                <Button
                                    size='large'
                                    formType='submit'
                                    type='primary'
                                >
                                    提交
                                </Button>
                                <Button
                                    size='large'
                                    formType='reset'
                                >
                                    重置
                                </Button>
                            </div>
                        </View>
                    }

                    onFinish={async (values) => {
                        console.log('Form onFinish values:', values);
                    }}
                    onFinishFailed={(values, errors) => {
                        console.log('Form onFinishFailed values:', values, 'errors:', errors);
                    }}
                >

                    <Form.Item
                        label='图片'
                        name='image'
                    >
                        <Uploader
                            url={'http://127.0.0.1:1333/api/app//user_center/attachment/upload'}
                            headers={
                                {
                                    'Content-Type': 'multipart/form-data',
                                    'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                }
                            }
                        />
                    </Form.Item>
                </Form>

                <View className={'column'}>

                    <Button
                        onClick={
                            () => {
                                Taro.chooseImage({
                                    success(res) {
                                        console.log('Taro.chooseImage res', res)
                                        const uploadTask = Taro.uploadFile({
                                            url: 'http://127.0.0.1:1333/api/app//user_center/attachment/upload',
                                            filePath: res.tempFilePaths[0],
                                            // @ts-ignore
                                            fileType: res.tempFiles[0].type,
                                            header: {
                                                'Content-Type': 'multipart/form-data',
                                                'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                            },
                                            formData: {
                                                'formData1': 'formData1_value'
                                            },
                                            name: 'file',
                                            fileName: res.tempFiles[0].originalFileObj?.name,
                                            withCredentials: false, // H5
                                            success(response: { errMsg: any; statusCode: number; data: string }) {
                                                console.log(' Taro.uploadFile success response', response);

                                            },
                                            fail(e: any) {
                                                console.log(' Taro.uploadFile fail', e);
                                            },
                                            complete(e: any) {
                                                console.log('beforeXhrUpload Taro.uploadFile complete', e);
                                            },
                                        })
                                        uploadTask.progress(
                                            (res: {
                                                progress: any
                                                totalBytesSent: any
                                                totalBytesExpectedToSend: any
                                            }) => {
                                                console.log(' Taro.uploadFile uploadTask progress', res);
                                            }
                                        )
                                    }
                                })
                            }
                        }
                    >
                        文件上传测试 使用Taro.uploadFile
                    </Button>

                    <Button
                        onClick={
                            () => {
                                Taro.chooseImage({
                                    success(res) {
                                        console.log('Taro.chooseImage res', res)

                                        let tempFile = res.tempFiles[0];
                                        console.log(' Taro.chooseImage res', tempFile);
                                        console.log(' Taro.chooseImage res originalFileObj', tempFile.originalFileObj);
                                        const formData = new FormData()
                                        console.log('Taro.getEnv()', Taro.getEnv())
                                        if (Taro.getEnv() === 'WEB') {
                                            // H5端转二进制上传
                                            console.log('Taro.getEnv() is Web/H5')

                                            const blobUrl = tempFile.path
                                            // 使用fetch API获取blob
                                            fetch(blobUrl)
                                                .then(response => response.blob())
                                                .then(blob => {
                                                    console.log('blob data', blob)
                                                    // 创建一个唯一的文件名
                                                    // const filename = 'blob_data.bin';

                                                    // 将blob添加到FormData中
                                                    formData.append('file', blob, tempFile.originalFileObj?.name);
                                                    // formData.append('file', blob, filename);
                                                    // formData.append('file', blob);

                                                    // 此时formData中包含了名为'file'的二进制表单项

                                                    // 如果需要发送formData，可以使用fetch或XMLHttpRequest
                                                    // fetch('/upload-endpoint', {
                                                    //     method: 'POST',
                                                    //     body: formData
                                                    // })
                                                    // .then(response => response.json())
                                                    // .then(data => console.log(data))
                                                    // .catch(error => console.error('Error:', error));
                                                    console.log('formData append file done', formData)
                                                })
                                                .catch(error => console.error('Error fetching blob:', error));
                                        }

                                        console.log('待提交 formData ', formData)

                                        // Taro无法上传文件 只能使用POST请求表单传输文件
                                        const requestTask = Taro.request({
                                            url: 'http://127.0.0.1:1333/api/app//user_center/attachment/upload',
                                            header: {
                                                'Content-Type': 'multipart/form-data',
                                                'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                            },
                                            method: 'POST',
                                            data: formData,
                                            success(response: { errMsg: any; statusCode: number; data: string }) {
                                                console.log(' Taro.request postFileData success response', response);

                                            },
                                            fail(e: any) {
                                                console.log(' Taro.request postFileData fail', e);
                                            },
                                            complete(e: any) {
                                                console.log('Taro.request postFileData complete', e);
                                            },
                                        })

                                        console.log('requestTask', requestTask)

                                        // Taro.request 不能正确发送 formData
                                        fetch('http://127.0.0.1:1333/api/app//user_center/attachment/upload', {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'multipart/form-data',
                                                'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                            },
                                            body: formData,
                                        })
                                            .then(response => response.json())
                                            .then(data => console.log('fetch post formData response data', data))
                                            .catch(error => console.error('Error:', error));


                                    }
                                })
                            }
                        }
                    >
                        文件上传测试 使用Taro.uploadFile 文件路径转二进制
                    </Button>

                    <Button
                        onClick={
                            () => {
                                Taro.chooseImage({
                                    success(res) {
                                        console.log('Taro.chooseImage res', res)

                                        let tempFile = res.tempFiles[0];
                                        console.log(' Taro.chooseImage res', tempFile);
                                        console.log(' Taro.chooseImage res originalFileObj', tempFile.originalFileObj);

                                        console.log('Taro.getEnv()', Taro.getEnv())
                                        if (Taro.getEnv() === 'WEB') {
                                            // H5端转二进制上传 当前使用axios上传
                                            console.log('Taro.getEnv() is Web/H5')

                                            // 创建FormData对象
                                            const formData = new FormData();
                                            // @ts-ignore
                                            formData.append('file', tempFile.originalFileObj); // 将文件添加到FormData中，字段名为'file'
                                            formData.append('field_test1', '测试多余自定义字段值')

                                            axios.post('http://127.0.0.1:1333/api/app//user_center/attachment/upload', formData, {
                                                method: 'POST',
                                                headers: {
                                                    'Content-Type': 'multipart/form-data',
                                                    'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                                },
                                                // body: formData,
                                            })
                                                .then(data => console.log('axios post formData response data', data))
                                                .catch(error => console.error('Error:', error));
                                        }
                                        // 其它非Web平台 使用默认方法
                                    }
                                })
                            }
                        }
                    >
                        文件上传测试 使用 axios formData上传
                    </Button>


                </View>

                {/*H5原生文件选择 测试*/}
                <div>
                    H5文件输入框fetch上传测试
                </div>
                <input
                    type={'file'}
                    onChange={
                        (e) => {
                            console.log('input file onChange e', e);
                            console.log('input file onChange e.nativeEvent?.target', e.nativeEvent?.target);
                            console.log('input file onChange e.target?.files', e.target?.files);

                            if (e.target?.files && e.target?.files?.length > 0) {
                                const file = e.target.files[0]; // 获取文件

                                // 创建FormData对象
                                const formData = new FormData();
                                formData.append('file', file); // 将文件添加到FormData中，字段名为'file'

                                fetch('http://127.0.0.1:1333/api/app//user_center/attachment/upload', {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'multipart/form-data',
                                        'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                    },
                                    body: formData,
                                })
                                    .then(response => response.json())
                                    .then(data => console.log('fetch post formData response data', data))
                                    .catch(error => console.error('Error:', error));

                            }
                        }
                    }
                />
                <div>
                    H5文件输入框axios上传测试
                </div>
                <input
                    type={'file'}
                    onChange={
                        (e) => {
                            console.log('input file onChange e', e);
                            console.log('input file onChange e.nativeEvent?.target', e.nativeEvent?.target);
                            console.log('input file onChange e.target?.files', e.target?.files);

                            if (e.target?.files && e.target?.files?.length > 0) {
                                const file = e.target.files[0]; // 获取文件

                                // 创建FormData对象
                                const formData = new FormData();
                                formData.append('file', file); // 将文件添加到FormData中，字段名为'file'
                                formData.append('field_test1', '测试多余自定义字段值')

                                axios.post('http://127.0.0.1:1333/api/app//user_center/attachment/upload', formData, {
                                    method: 'POST',
                                    headers: {
                                        'Content-Type': 'multipart/form-data',
                                        'Ann-Token': 'eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjIsImlwIjoiMTI3LjAuMC4xIiwiZXhwIjoxNzE4ODc0ODIwfQ.3l--LsDxQOMR3MJ6QbHxhMIAz30vWXwvQkKstuhfEKw',
                                    },
                                    // body: formData,
                                })
                                    .then(data => console.log('axios post formData response data', data))
                                    .catch(error => console.error('Error:', error));

                            }
                        }
                    }
                />

                <View
                    style={
                        {
                            position: 'sticky',
                            top: 0,
                            flex: 1,
                            backgroundColor: "red",
                        }
                    }
                >
                    <Button
                        onClick={() => setPopupViewVisible(true)}
                    >
                        开Popup 筛选
                    </Button>


                    <Popup
                        visible={popupViewVisible}
                        lockScroll
                        closeable
                        position='bottom'
                        style={{height: '30%'}}
                        onClose={() => {
                            console.log('onClose')
                            setPopupViewVisible(false)
                        }}
                    >

                    </Popup>
                </View>

                <View>
                    {
                        new Array(5).fill(0).map((_item, index) => (
                            <View
                                key={index}
                            >
                                <Image
                                    width='200px'
                                    height='200px'
                                    src={'https://gitee.com/static/images/logo-black.svg?t=158106664'}
                                />

                                <Avatar
                                    size='24'
                                />
                                <TaroImage
                                    src={'https://foruda.gitee.com/avatar/1676920109609176416/569171_jiankian_1578927932.png!avatar200'}
                                />
                            </View>
                        ))
                    }


                </View>


                <View
                    style={
                        {
                            height: '100px'
                        }
                    }
                >
                    底部隔离
                </View>

            </View>
            <Tabbar fixed>
                <Tabbar.Item title="首页"/>
                <Tabbar.Item title="分类"/>
                <Tabbar.Item title="发现"/>
                <Tabbar.Item title="购物车"/>
                <Tabbar.Item title="我的"/>
            </Tabbar>
        </View>
    )
}

export default Index
